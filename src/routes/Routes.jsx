import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Home } from '../pages/Home';
import { AboutMe } from '../pages/AboutMe';
import { Contact } from '../pages/Contact';
import { Project } from '../pages/Project';
import { ProjectDetail } from '../pages/ProjectDetail';

const Routes = () => {
  return ( 
    <Switch>
        <Route path="/" exact={true} component={Home}/>
        <Route path="/about-me" component={AboutMe}/>
        <Route path="/project" component={Project}/>
        <Route path="/detail" component={ProjectDetail}/>
        <Route path="/contact" component={Contact}/>
        {/* <Route component={Error404} /> */}
    </Switch>
   );
}
 
export default Routes;
import React from 'react';
import styled, {css} from 'styled-components';
import { NavLink } from 'react-router-dom';
import { device } from '../elements/device';
import { useTranslation } from "react-i18next";
import One from '../img/projects/Slide.png';
import Six from '../img/projects/Banner.png';

export const Project = () => {
    const [t] = useTranslation("global");

    return (
        <>
            <Desk>
                <ContenedorProjectLeft>
                    <BoxLeft>
                        <Box one>
                            <NavLink to="/detail">
                                <More> {t("button.view__project")} </More> 
                            </NavLink>
                        </Box>

                        <Box four>
                            <NavLink to="/detail">
                                <More> {t("button.view__project")} </More> 
                            </NavLink>
                        </Box>

                        <Box five>
                            <NavLink to="/detail">
                                <More> {t("button.view__project")} </More> 
                            </NavLink>
                        </Box>
                    </BoxLeft>

                    <BoxRight>
                        <Box two>
                            <NavLink to="/detail">
                                <More> {t("button.view__project")} </More> 
                            </NavLink>
                        </Box>

                        <Box three>
                            <NavLink to="/detail">
                                <More> {t("button.view__project")} </More> 
                            </NavLink>
                        </Box>

                        <Box six>
                            <NavLink to="/detail">
                                <More> {t("button.view__project")} </More> 
                            </NavLink>
                        </Box>
                    </BoxRight>
                </ContenedorProjectLeft>

                <ContenedorProjectRight>
                    <ProjectTitle  className="textoG"> {t("projects.title")} </ProjectTitle>
                    <Parrafo>{t("projects.subtitle")}</Parrafo>
                    
                </ContenedorProjectRight>
            </Desk>

            <Mobile>
                <ContenedorProjectLeft>
                    <ProjectTitle  className="textoG"> {t("projects.title")} </ProjectTitle>
                    <Parrafo>{t("projects.subtitle")}</Parrafo>
                </ContenedorProjectLeft>

                <ContenedorProjectRight>
                    <BoxLeft>
                        <NavLink to="/detail">
                            <Box one />
                        </NavLink>

                        <NavLink to="/detail">
                            <Box four />
                        </NavLink>

                        <NavLink to="/detail">
                            <Box five />
                        </NavLink>
                    </BoxLeft>

                    <BoxRight>
                        <NavLink to="/detail">
                            <Box two />
                        </NavLink>

                        <NavLink to="/detail">
                            <Box three />
                        </NavLink>

                        <NavLink to="/detail">
                            <Box six />
                        </NavLink>
                    </BoxRight>
                </ContenedorProjectRight>
            </Mobile>
        </>
    )
}

const More = styled.span `
    border: 0px;
    border-bottom: 1px solid;
    border-image: var(--linear);
    border-image-slice: 1;
    bottom: 10px;
    color: #070918;
    cursor: pointer;
    display: none;
    float: right;
    font-size: 10px;
    margin: auto;
    padding-bottom: 3px;
    right: 15px;
    letter-spacing: 0.15em;
    text-transform: uppercase;
    position: absolute;
`;

const Box = styled.article `
    margin-bottom: 3px;
    margin-right: 3px;
    position: relative;

    ${props => props.one && css`
        background: var(--linear);
        background-image: url(${One});
        background-position: 50% 50%;
        background-repeat: no-repeat;
        background-size: 100% 100%;
        height: 40%;

        @media ${device.phoneLand} {
            border-radius: 50px 0 0 0;
            height: 30%;
        }
    `}

    ${props => props.two && css`
        background: var(--linear);
        background-image: url(${One});
        background-position: 50% 50%;
        background-repeat: no-repeat;
        background-size: 100% 100%;
        height: 30%;

        @media ${device.phoneLand} {
            border-radius: 0 50px 0 0;
            height: 25%;
        }
    `}

    ${props => props.three && css`
        background: var(--linear);
        background-image: url(${One});
        background-position: 50% 50%;
        background-repeat: no-repeat;
        background-size: 100% 100%;
        height: 30%;
        @media ${device.phoneLand} {
            height: 25%;
        }
    `}

    ${props => props.four && css`
        background: var(--linear);
        background-image: url(${One});
        background-position: 50% 50%;
        background-repeat: no-repeat;
        background-size: 100% 100%;
        height: 30%;
        @media ${device.phoneLand} {
            height: 25%;
        }
    `}

    ${props => props.five && css`
        background: var(--linear);
        background-image: url(${One});
        background-position: 50% 50%;
        background-repeat: no-repeat;
        background-size: 100% 100%;
        height: 30%;
        @media ${device.phoneLand} {
            height: 25%;
        }
    `}

    ${props => props.six && css`
        background: var(--linear);
        background-image: url(${Six});
        background-position: 50% 50%;
        background-repeat: no-repeat;
        background-size: 100% 100%;
        height: 40%;
        @media ${device.phoneLand} {
            height: 30%;
        }
    `}

    &:hover ${More} {
        display: block;
    }
`;

const BoxRight = styled.div `
    float: right;
    height: 98.7vh;
    width: 50%;

    @media ${device.phoneLand} { 
        height: 86vh;
    }
`;

const BoxLeft = styled.div `
    float: left;
    height: 98.7vh;
    width: 50%;

    @media ${device.phoneLand} { 
        height: 86vh;
    }
`;

const ContenedorProjectLeft = styled.div `
    float: left;
    margin-left: -24px;
    margin-top: -88px;
    position: relative;
    text-align: center;
    width: 50%;

    @media ${device.phoneLand} { 
        width: 100%;
        height: 50%;
        left: 24px;
        margin-top: -80px;
    }
`;

const ContenedorProjectRight = styled.div `
    float: right;
    height: 100%;
    text-align: center;
    text-transform: uppercase;
    width: 50%;

    @media ${device.phoneLand} { 
        width: 100%;
        height: 80vh;
        margin-top: 55%;
        left: 0;
        background: var(--fondoNav);
        position: absolute;
        border-radius: 50px 50px 0 0;
    }
`;

const Desk = styled.div `
    display: block;

    @media ${device.phoneLand} { 
        display: none;
    }
`;

const Mobile = styled.div `
    display: none;

    @media ${device.phoneLand} { 
        display: block;
    }
`;

const Parrafo = styled.p `
    font-size: 13px;
    font-weight: 400;
    letter-spacing: 0.15em;
    text-transform: uppercase;

    ${props => props.tag && css`
        font-size: 16px;   
    `}
`;

const ProjectTitle = styled.h1 `
    font-size: 35px;
    font-weight: 400;
    letter-spacing: 0.15em;
    margin-top: 30%;
    text-transform: uppercase;
    @media ${device.phoneLand} {
        font-size: 30px;
    }
`;
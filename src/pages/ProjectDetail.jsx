import React from 'react';
import styled from 'styled-components';
import { device } from '../elements/device';
import { FaCodepen, FaBitbucket } from "react-icons/fa";
import { AiOutlineLinkedin } from "react-icons/ai";
import { useTranslation } from "react-i18next";
import Boton from '../elements/Boton';
import One from '../img/projects/Slide.png';
import Bg from '../img/projects/bg.svg';


export const ProjectDetail = () => {
    const [t] = useTranslation("global");


    return (
        <>
            <Desk>
                <ContenedorDetailLeft>
                    <Centered>
                        <DetailNro>01.</DetailNro>
                        <DetailTitle className="textoG">Pokedex</DetailTitle>
                        

                        <Description>
                            <h2>{t("detail__project.description")}</h2>
                            <Parrafo>Desart is a powerful, clean, creative and easy to use Web Design Studio PSD Template. It is highly suitable template for companies that offer Web </Parrafo>

                            <Parrafo>Desart is a powerful, clean, creative and easy to use Web Design Studio PSD Template. It is highly suitable template for companies that offer Web </Parrafo>
                        </Description>

                        <Tools>
                            <h2>{t("detail__project.tools")}</h2>
                            <FaCodepen/>
                            
                            <AiOutlineLinkedin/>

                            <FaBitbucket/>
                        </Tools>

                        <Boton><span>{t("button.demo")}</span></Boton>
                    </Centered>
                </ContenedorDetailLeft>

                <ContenedorDetailRight>
                    <Borde/>
                    <Imagen/>
                    <Bg_img/>
                </ContenedorDetailRight>
            </Desk>
            <Mobile>
                <ContenedorDetailLeft>
                    <Borde/>
                    <Imagen/>
                    <Bg_img/>
                </ContenedorDetailLeft>
                <ContenedorDetailRight>
                    <Centered>
                        <DetailNro>01.</DetailNro>
                        <DetailTitle className="textoG">Pokedex</DetailTitle>
                        

                        <Description>
                            <h2>{t("detail__project.description")}</h2>
                            <Parrafo>Desart is a powerful, clean, creative and easy to use Web Design Studio PSD Template. It is highly suitable template for companies that offer Web </Parrafo>

                            <Parrafo>Desart is a powerful, clean, creative and easy to use Web Design Studio PSD Template. It is highly suitable template for companies that offer Web </Parrafo>
                        </Description>

                        <Tools>
                            <h2>{t("detail__project.tools")}</h2>
                            <FaCodepen/>
                            
                            <AiOutlineLinkedin/>

                            <FaBitbucket/>
                        </Tools>

                        <Boton><span>{t("button.demo")}</span></Boton>
                    </Centered>
                </ContenedorDetailRight>
            </Mobile>
        </>
    )
}

const Bg_img = styled.div `
    background-image: url(${Bg});
    background-position: 50% 50%;
    background-repeat: no-repeat;
    background-size: 100% 100%;
    float: right;
    height: 60%;
    margin-top: 5%;
    position: absolute;
    width: 40%;
    right: 2%;

    @media ${device.tabLand} {
        display: none
    }
`;

const Borde = styled.div `
    background-clip: content-box, border-box;
    background-image: var(--btnLinear);
    background-origin: border-box;
    border: double 2px transparent;
    border-radius: 5px;
    color: var(--texto);
    float: right;
    height: 50vh;
    outline:none;
    width: 90%;
    @media ${device.phoneLand} {
        float: inherit;
        margin: 0 auto;
        margin-top: 15%;
        height: 35vh;
    }
`;

const Centered = styled.div `
    margin-left: 0;    
    margin-top: 0;

    @media ${device.phoneLand} {
        margin-left: 24px;
        margin-right: 24px;
        margin-top: 10%;
    }
`;

const ContenedorDetailLeft = styled.div `
    float: left;
    height: 100%;
    width: 50%;

    @media ${device.phoneLand} { 
        width: 100vw;
        height: 50%;
        left: 0;
        margin-top: -90px;
        position: absolute;
    }
`;

const ContenedorDetailRight = styled.div `
    float: right;
    height: 100%;
    width: 50%;

    @media ${device.phoneLand} { 
        width: 100%;
        height: 100vh;
        margin-top: 290px;
        left: 0;
        background: var(--fondoNav);
        position: absolute;
        border-radius: 50px 50px 0 0;
    }
`;

const Description = styled.div `
    h2 {
        color: var(--muted);
        font-size: 14px;
        letter-spacing: 0.15em;
        margin-bottom: 15px;
        text-transform: uppercase;
    }
`;

const Desk = styled.div `
    display: block;

    @media ${device.phoneLand} { 
        display: none;
    }
`;

const DetailTitle = styled.h1 `
    font-size: 35px;
    font-weight: 400;
    letter-spacing: 0.15em;
    text-transform: uppercase;
    margin-bottom: 10%;
`;

const DetailNro = styled.span `
    color: var(--texto);
    font-size: 35px;
    font-weight: 200 !important;
    letter-spacing: 0.15em;
    text-transform: uppercase;
`;

const Imagen = styled.div `
    background: #F9C5FF;
    background-image: url(${One});
    background-position: 50% 50%;
    background-repeat: no-repeat;
    background-size: 100% 100%;
    float: right;
    height: 50vh;
    margin-top: -270px;
    position: relative;
    right: 3.8%;
    width: 80%;

    @media ${device.phoneLand} {
        height: 50%;
        right: 15%;
        margin-top: -230px;
        width: 70%;
    }
`;

const Mobile = styled.div `
    display: none;

    @media ${device.phoneLand} { 
        display: block;
    }
`;

const Parrafo = styled.p `
    font-size: 13px;
    font-weight: 400;
    letter-spacing: 0.15em;
    margin-top: 15px;

    @media ${device.phoneLand} { 
        width: 100%;
        text-align: justify;
        letter-spacing: 0.10em;
    }
`;

const Tools = styled.div `
    margin-top: 8%;
    margin-bottom: 5%;

    h2 {
        color: var(--muted);
        font-size: 14px;
        letter-spacing: 0.15em;
        margin-bottom: 15px;
        text-transform: uppercase;
    }

    svg {
        margin-right: 15px;
        font-size: 40px;
        cursor: pointer;
        background: var(--linear);
        color: var(--fondoNav);
        border-radius: 50%;
        padding: 5px;
        transition: all 0.3s ease 0s;
    }
`;
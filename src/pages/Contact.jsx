import React from 'react';
import styled from 'styled-components';
import { device } from '../elements/device';
import Link from '@material-ui/core/Link';
import { FaCodepen, FaBitbucket } from "react-icons/fa";
import { AiOutlineLinkedin, AiOutlineMail, AiOutlineMobile } from "react-icons/ai";
import TextField from '@material-ui/core/TextField';
import { useTranslation } from "react-i18next";
import Boton from '../elements/Boton';


export const Contact = () => {
    const [t] = useTranslation("global");


    return (
        <>
            <ContenedorContactLeft>
                <ContactTitle className="textoG">{t("contact.title")}</ContactTitle>
                <Parrafo>{t("contact.help")}</Parrafo>

                <ContactMe>
                    <div>
                        <AiOutlineMail/>
                        <div>
                            <p>{t("contact.mail")}</p>
                            <span>annynk.ramos@gmail.com</span>
                        </div>
                    </div>
                    <div>
                        <AiOutlineMobile/>
                        <div>
                            <p>{t("contact.phone")}</p>
                            <span>+58 412.406.51.34</span>
                        </div>
                    </div>
                </ContactMe>

                <MenuSocial>
                    <h2>{t("contact.meet")}</h2>
                    <Link href="https://codepen.io/annyramos/" color="inherit"  target="_blank">
                        <FaCodepen/>
                    </Link>
                    
                    <Link href="https://www.linkedin.com/in/annyramos/" color="inherit"  target="_blank">
                        <AiOutlineLinkedin/>
                    </Link>

                    <Link href="https://bitbucket.org/annynkr/" color="inherit"  target="_blank">
                        <FaBitbucket/>
                    </Link>
                </MenuSocial>
            </ContenedorContactLeft>

            <ContenedorContactRight>
                <Form validate autoComplete="off">
                    <TextField 
                        id="name" 
                        label={t("contact.name")} 
                        variant="outlined" 
                    />

                    <TextField 
                        id="mail" 
                        label={t("contact.mail")} 
                        type="email"
                        variant="outlined" 
                    />

                    <TextField 
                        id="message" 
                        label={t("contact.message")} 
                        variant="outlined" 
                        rows={4} 
                        multiline
                    />

                    <Boton><span>{t("button.send")}</span></Boton>
                </Form>
            </ContenedorContactRight>

            <ContenedorContactMobile>
                <ContactTitle className="textoG">{t("contact.title")}</ContactTitle>
                <Parrafo>{t("contact.help")}</Parrafo>

                <Form validate autoComplete="off">
                    <TextField 
                        id="name" 
                        label={t("contact.name")} 
                        variant="outlined" 
                    />

                    <TextField 
                        id="mail" 
                        label={t("contact.mail")} 
                        type="email"
                        variant="outlined" 
                    />

                    <TextField 
                        id="message" 
                        label={t("contact.message")} 
                        variant="outlined" 
                        rows={4} 
                        multiline
                    />

                    <Boton><span>{t("button.send")}</span></Boton>
                </Form>

                <ContactMe>
                    <div>
                        <AiOutlineMail/>
                        <div>
                            <p>{t("contact.mail")}</p>
                            <span>annynk.ramos@gmail.com</span>
                        </div>
                    </div>
                    <div>
                        <AiOutlineMobile/>
                        <div>
                            <p>{t("contact.phone")}</p>
                            <span>+58 412.406.51.34</span>
                        </div>
                    </div>
                </ContactMe>

                <MenuSocial>
                    <h2>{t("contact.meet")}</h2>
                    <Link href="https://codepen.io/annyramos/" color="inherit"  target="_blank">
                        <FaCodepen/>
                    </Link>
                    
                    <Link href="https://www.linkedin.com/in/annyramos/" color="inherit"  target="_blank">
                        <AiOutlineLinkedin/>
                    </Link>

                    <Link href="https://bitbucket.org/annynkr/" color="inherit"  target="_blank">
                        <FaBitbucket/>
                    </Link>
                </MenuSocial>
            </ContenedorContactMobile>
        </>
    )
}

const ContactTitle = styled.h1 `
    font-size: 35px;
    font-weight: 400;
    letter-spacing: 0.15em;
    text-transform: uppercase;
`;

const ContenedorContactLeft = styled.div `
    float: left;
    height: 100%;
    width: 50%;

    @media ${device.phoneLand} { 
        display: none; 
    }
`;

const ContenedorContactRight = styled.div `
    float: right;
    height: 100%;
    width: 50%;

    @media ${device.phoneLand} { 
        display: none;
    }
`;

const ContenedorContactMobile = styled.div `
    height: 135vh;
    width: 100%;
    display: none;

    @media ${device.phoneLand} { 
        display: block;
    }
`;

const Parrafo = styled.p `
    font-size: 13px;
    font-weight: 400;
    letter-spacing: 0.15em;
    margin-top: 15px;
`;

const ContactMe = styled.div `
    margin-top: 10%;
    
    div {
        display: flex;
        margin-bottom: 20px;
    }

    div > div {
        display: block;
    }
    
    p {
        color: var(--muted);
        font-size: 12px;
        letter-spacing: 0.15em;
    }

    span {
        font-size: 13px;
        letter-spacing: 0.15em;
    }

    svg {
        margin-right: 15px;
        font-size: 35px;
        cursor: pointer;
        background: var(--linear);
        color: var(--fondoNav);
        border-radius: 50%;
        padding: 5px;
        transition: all 0.3s ease 0s;
    }

    @media ${device.phoneLand} { 
        margin-top: 150%;
    }
`;

const MenuSocial = styled.div `
    margin-top: 10%;

    h2 {
        color: var(--muted);
        font-size: 14px;
        letter-spacing: 0.15em;
        margin-bottom: 15px;
        text-transform: uppercase;
    }

    svg {
        margin-right: 15px;
        font-size: 40px;
        cursor: pointer;
        background: var(--linear);
        color: var(--fondoNav);
        border-radius: 50%;
        padding: 5px;
        transition: all 0.3s ease 0s;
    }
`;

const Form = styled.form `
    width: 80%;
    float: right;
    margin-top: 40px;
    
    &.input {
        width: 100%;
    }

    @media ${device.phoneLand} { 
        width: 100%; 
    }
`;
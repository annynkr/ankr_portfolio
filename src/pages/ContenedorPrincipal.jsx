import React from 'react';
import styled from 'styled-components';
import { NavLink } from 'react-router-dom';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Divider from '@material-ui/core/Divider';
import Link from '@material-ui/core/Link';
import IconButton from '@material-ui/core/IconButton';
import { MdMenu,  MdClear } from "react-icons/md";
import { FaCodepen, FaBitbucket } from "react-icons/fa";
import { AiOutlineLinkedin } from "react-icons/ai";
import { useTranslation } from "react-i18next"
import { FooterTop } from '../components/footer/FooterTop';
import { LanguagueMenu } from '../components/sidebarTopNav/LanguagueMenu';
import MenuMobile from '../components/sidebarTopNav/MenuMobile';
import { device } from '../elements/device';
import { Footer } from '../components/footer/Footer';
import Routes from '../routes/Routes';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
}));

export default function ContenedorPrincipal() {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const [t] = useTranslation("global");

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const [colorChange, setColorchange] = React.useState(false);
  const changeNavbarColor = () =>{
     if(window.scrollY >= 80){
       setColorchange(true);
     }
     else{
       setColorchange(false);
     }
  };
  window.addEventListener('scroll', changeNavbarColor);

  return (
    <div className={classes.root}>

      <AppBar
          position="fixed"
          className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
          })}
      >
          <Toolbar className={colorChange ? 'MuiPaper-root colorChange' : 'MuiPaper-root'}>
            <ContenedorMenuDesktop>
                <IconButton
                color="inherit"
                aria-label="open drawer"
                onClick={handleDrawerOpen}
                edge="start"
                className={clsx(classes.menuButton, open && classes.hide)}
                >
                <MdMenu />
                </IconButton>
            </ContenedorMenuDesktop>
            
            <ContenedorMenuMobile>
                <FooterTop/>
            </ContenedorMenuMobile>

            <LanguagueMenu/>
          </Toolbar>
      </AppBar>
      
      <ContenedorMenuDesktop>
          <Drawer
          className={classes.drawer}
          variant="persistent"
          anchor="left"
          open={open}
          classes={{
              paper: classes.drawerPaper,
          }}
          >
          <div className={classes.drawerHeader}>
              <IconButton onClick={handleDrawerClose}>
              {theme.direction === 'ltr' ? <MdClear /> : <MdClear />}
              </IconButton>
          </div>
          
          <Divider />

          <ListaMenu>
              <NavLink to="/" exact={true}>{t("sidebar.home")}</NavLink>
              <NavLink to="/about-me">{t("sidebar.about__me")}</NavLink>
              <NavLink to="/project">{t("sidebar.projects")}</NavLink>
              <NavLink to="/contact">{t("sidebar.contact")}</NavLink>
          </ListaMenu>

          <MenuSocial>
              <Link href="https://codepen.io/annyramos/" color="inherit"  target="_blank">
                  <FaCodepen/>
              </Link>
              
              <Link href="https://www.linkedin.com/in/annyramos/" color="inherit"  target="_blank">
                  <AiOutlineLinkedin/>
              </Link>

              <Link href="https://bitbucket.org/annynkr/" color="inherit"  target="_blank">
                  <FaBitbucket/>
              </Link>
          </MenuSocial>
          
          </Drawer>
      </ContenedorMenuDesktop>
          
      <ContenedorMenuMobile>
          <MenuMobile/>
      </ContenedorMenuMobile>
      
      <main
        className={clsx(classes.content, {
          [classes.contentShift]: open,
        })}
      >
        <div className={classes.drawerHeader} />
        <Routes/>

        <Footer/>
      </main>
    </div>
  );
}

const ContenedorMenuDesktop = styled.div `
  display: block;

  @media ${device.phoneLand} { 
      display: none;
  }
`;

const ContenedorMenuMobile = styled.div `
  display: none;

  @media ${device.phoneLand} { 
      display: block;
  }
`;

const ListaMenu = styled.nav `
  list-style: none;
  padding: 10px;
  text-align: center;
  position: relative;
  margin: 0 auto;
  top: 30%;
  cursor: pointer;

  a {
    margin-top: 10px;
    text-decoration: none;
    text-transform: uppercase;
    display: block;
    color: var(--texto);
    
    &:hover,
    &.active{
        background:  var(--linear);
        -webkit-background-clip: text;
        background-clip: text;
        color: transparent;
        -webkit-text-fill-color: transparent;
        color: var(--primary);
        transition: all 0.3s ease 0s;
    }
  }
`;

const MenuSocial = styled.div `
    position: absolute;
    bottom: 20px;
    margin: 0 0 0 5%;

    svg {
        margin-left: 15px;
        margin-right: 15px;
        font-size: 40px;
        cursor: pointer;
        background: var(--linear);
        border-radius: 50%;
        color: #080C1E;
        padding: 5px;
        transition: all 0.3s ease 0s;
    }
`;
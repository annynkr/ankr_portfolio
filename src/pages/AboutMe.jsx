import React from 'react';
import styled, {css} from 'styled-components';
import { device } from '../elements/device';
import { useTranslation } from "react-i18next";
import Boton from '../elements/Boton';
import ImgOne from '../img/about-me/PhotoOne.jpg';

export const AboutMe = () => {
    const [t] = useTranslation("global");

    return (
        <>
            <ContenedorAboutLeft>
                <PhotoOne/>
            </ContenedorAboutLeft>

            <ContenedorAboutRight>
                <Centered>
                    <AboutTitle>{t("about.hello")}</AboutTitle>
                    <Hr/>
                    <Parrafo myName>{t("about.my__name")}</Parrafo>
                    <Parrafo description>{t("about.presentation")}</Parrafo>
                    <Boton><span>{t("button.cv")}</span></Boton>
                </Centered>
            </ContenedorAboutRight>
        </>
    )
}

const AboutTitle = styled.h1 `
    font-size: 60px;
    font-weight: 400;
    letter-spacing: 0.15em;
    margin-top: 30px;
    
    @media ${device.phoneLand} {
        font-size: 40px;
    }
    
`;

const Centered = styled.div `
    margin-left: 15%;    
    margin-top: 25%;

    @media ${device.phoneLand} {
        margin-left: 24px;
        margin-right: 24px;
        margin-top: 10%;
    }
`;

const ContenedorAboutLeft = styled.div `
    float: left;
    height: 100vh;
    width: 50%;
    margin-top: -88px;
    position: relative;

    @media ${device.phoneLand} { 
        width: 100%;
        height: 50%;
        left: 0;
        margin-top: -80px;
        position: absolute;
    }
`;

const ContenedorAboutRight = styled.div `
    float: right;
    height: 100vh;
    width: 50%;
    margin-left: -24px;
    margin-top: -88px;

    @media ${device.phoneLand} { 
        width: 100%;
        height: 80vh;
        left: 24px;
        background: var(--fondoNav);
        position: absolute;
        border-radius: 50px 50px 0 0;
        margin-top: 210px;
    }
`;

const Hr = styled.hr `
    background-image: var(--linear);
    border:none;
    color: var(--linear);
    height: 1.5px;
    margin-top: 0;
    margin-right: 5px;
    width: 8%;
    display: inline-flex;
`;

const Parrafo = styled.p `
    font-size: 13px;
    font-weight: 200;
    letter-spacing: 0.15em;

    ${props => props.myName && css`
        display: inline-flex;
        margin: 0 auto;
        margin-bottom: 50px;
        width: 70%;
    `}

    ${props => props.description && css`
        margin-bottom: 25px;
        text-align: justify;
        width: 80%;
        @media ${device.phoneLand} {
            width: 100%;
            letter-spacing: 0.10em;
        }
    `}
`;

const PhotoOne = styled.div `
    background-color: gray;
    background-image: url(${ImgOne});
    background-position: 50% 50%;
    background-repeat: no-repeat;
    background-size: cover;
    height: 100%;
    width: 70%;

    @media ${device.phoneLand} {
        width: 100%;
    }
`;
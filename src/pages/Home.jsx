import React from 'react';
import styled from 'styled-components';
import { device } from '../elements/device';
import { useTranslation } from "react-i18next";

export const Home = () => {
    const [t] = useTranslation("global");

    return (
        <ContenedorTitulo>
            <Titulo>
                <HeaderTitulo className="textoG">ANNY RAMOS</HeaderTitulo>
            </Titulo>
            <HeaderSubtitulo>{t("home.subtitle")}</HeaderSubtitulo>
        </ContenedorTitulo>
    )
}

const ContenedorTitulo = styled.div `
    left: 24px;
    margin: 0 auto;
    position: relative;
    right: 24px;
    text-align: center;
    top:30%;
    transition: all 0.3s ease 0s;

    @media ${device.phoneLand} { 
        left: 0;
        top:40%;
    }
`;

const HeaderSubtitulo = styled.p `
    font-size: 18px;
    font-weight: 400;
    letter-spacing: 0.15em;
`;

const HeaderTitulo = styled.span `
    font-size: 70px;

  @media ${device.phoneLand} { 
    font-size: 30px;
  }
`;

const Titulo = styled.h1 `
    font-style: normal;
    font-weight: 500;
    letter-spacing: 0.15em;
    text-transform: uppercase;
    vertical-align: middle;
`;
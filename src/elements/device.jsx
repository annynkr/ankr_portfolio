const size = {
    phone: '575px',
    phoneLand: '767px',
    tabPort: '991px',
    tabLand: '1199px',
    desktop: '1200px',
    bigDesktop: '1800px'


  }

export const device = {
    phone: `(max-width: ${size.phone})`,
    phoneLand: `(max-width: ${size.phoneLand})`,
    tabPort: `(max-width: ${size.tabPort})`,
    tabLand: `(max-width: ${size.tabLand})`,
    desktop: `(min-width: ${size.desktop})`,
    bigDesktop: `(min-width: ${size.bigDesktop})`
};
    
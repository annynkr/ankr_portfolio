import styled, {css} from 'styled-components';

const Boton = styled.button`
    background-clip: content-box, border-box;
    background-image: var(--btnLinear);
    background-origin: border-box;
    border: double 1px transparent;
    border-radius: .75em;
    color: var(--texto);
    cursor: pointer;
    font-size: 13px;
    height: 50px;
    outline:none;
    text-transform: uppercase;
    transition: transform .25s;
    width: 150px;

    &:hover {
        background-image: var(--hBtnLinear);
        border: double 1px transparent;
        border-radius: .75em;
        transition: transform .25s;

        span {
            background:  var(--hLinear);
            -webkit-background-clip: text;
            background-clip: text;
            color: transparent;
            -webkit-text-fill-color: transparent;
            color: var(--primary);
            transition: all 0.3s ease 0s;
        }
    }

    &:active {
        transform: scale(.96);
    }

    span {
        background:  var(--linear);
        -webkit-background-clip: text;
        background-clip: text;
        color: transparent;
        -webkit-text-fill-color: transparent;
        color: var(--primary);
        transition: all 0.3s ease 0s;
    }

    ${props => props.abo && css`
        margin-left: 130px;
    `}

    ${props => props.lange && css`
        background: transparent;
        color: var(--texto);
        right: 20px;
        float: right;
        width: 50px;
        padding: 12px;
        font-size: 16px;
        line-height: normal;
        text-align: center;
        right: 10px;
        position: absolute;
        margin-top: -27px;
        border-radius: 50% !important;
        
        &:hover {
            background-image: none;
        }

        &:active {
            background-color: rgba(255, 255, 255, 0.4);
        }
    `}
`;

export default Boton;
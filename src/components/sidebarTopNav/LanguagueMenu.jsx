import i18next from 'i18next';
import React, { useState } from 'react';
import Boton from '../../elements/Boton';

export const LanguagueMenu = () => {
  const [languague, setLanguague] = useState (true);

  const handleClick = () => {
    setLanguague(!languague);
    languague? i18next.changeLanguage("en") : i18next.changeLanguage("es")
  }
  
  return (
    <div>
        <Boton lange onClick={handleClick}>
            {languague ? 'EN' : 'ES'}
        </Boton>
    </div>
  );
}


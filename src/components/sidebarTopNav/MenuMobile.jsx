import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import { IoMdHome, IoMdMoon, IoIosRocket, IoIosPhonePortrait } from "react-icons/io";
import { useTranslation } from "react-i18next";
import { Link } from 'react-router-dom';

const useStyles = makeStyles({
  root: {
    width: '100%',
  }, 
});

export default function MenuMobile() {
  const classes = useStyles();
  const [value, setValue] = useState('home');

  const [t] = useTranslation("global");

  const handleChange = (e, newValue) => {
    setValue(newValue);
  };

  return (
    <BottomNavigation value={value} onChange={handleChange} className={classes.root}>
        <BottomNavigationAction
            label={t("sidebar.home")}
            value="home"
            icon={<IoMdHome/>}
            component={Link}
            to="/"
        />
        <BottomNavigationAction 
            label={t("sidebar.about__me")}
            value="about"
            icon={<IoMdMoon/>}
            component={Link}
            to="/about-me"
        />
        <BottomNavigationAction 
            label={t("sidebar.projects")} 
            value="projects" 
            icon={<IoIosRocket/>}
            component={Link}
            to="/project"
        />
        <BottomNavigationAction 
            label={t("sidebar.contact")} 
            value="contact" 
            icon={<IoIosPhonePortrait/>} 
            component={Link}
            to="/contact"
        />
    </BottomNavigation>
  );
}

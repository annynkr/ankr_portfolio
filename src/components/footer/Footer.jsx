import React from 'react';
import styled from 'styled-components';
import { device } from '../../elements/device';
import { useTranslation } from "react-i18next"

export const Footer = () => {
    const [t] = useTranslation("global")

    return (
        <ContenedorFooter>
            <TitleFooter className="textoG">
                {t("footer.copyright")} {(new Date().getFullYear())}
            </TitleFooter>
        </ContenedorFooter>
    )
}

const ContenedorFooter = styled.footer `
    position: fixed;
    bottom: 0;
    height: 40px;
    right: 0;
    cursor: default;
    display: flex;

    @media ${device.phoneLand} { 
        display: none;
    }
`;

const TitleFooter = styled.span `
    font-size: 13px;
    line-height: 20px;
    align-items: center;
    letter-spacing: 0.05em;
    text-transform: uppercase;
    width: 100%;
    margin-right: 24px;
`;

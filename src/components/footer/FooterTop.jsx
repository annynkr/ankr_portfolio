import React from 'react';
import styled from 'styled-components';
import { device } from '../../elements/device';
import { useTranslation } from "react-i18next"

export const FooterTop = () => {
    const [t] = useTranslation("global")

    return (
        <ContenedorFooterMobile>
            <TitleFooterMobile className="textoG">
                {t("footer.copyright")} {(new Date().getFullYear())}
            </TitleFooterMobile>
        </ContenedorFooterMobile>
    )
}

const ContenedorFooterMobile = styled.footer `
    position: fixed;
    top: 17px;
    left: 0;
    height: 50px;
    display: none;
    
    @media ${device.phoneLand} { 
        display: flex;
    }
`;

const TitleFooterMobile = styled.span `
    font-size: 13px;
    align-items: center;
    letter-spacing: 0.05em;
    text-transform: uppercase;
    width: 100%;
    margin-left: 24px;
`;

import React from "react";
import { BrowserRouter } from 'react-router-dom';
import ContenedorPrincipal from "./pages/ContenedorPrincipal";

const App = () => {
  return (
    <BrowserRouter>
      <ContenedorPrincipal />
    </BrowserRouter>
  );
}

export default App;